#include "Customer.h"

Customer::Customer(std::string aName) :
	name(aName)
{
}
Customer::Customer(){
}
Customer::~Customer()
{
	;
}

std::string Customer::getName()
{
	return name;
}

bool Customer::isMember()
{
	return member;
}

void Customer::setMember(bool member)
{
	this->member = member;
}

std::string Customer::getMemberType()
{
	return memberType;
}
void Customer::setMemberType(std::string type)
{
	if(member)
		memberType = type;
}

std::string Customer::toString()
{
	return this->name.append(" Membership: ").append(memberType);
}


