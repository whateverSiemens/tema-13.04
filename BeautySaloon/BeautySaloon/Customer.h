#pragma once
#include <string>


class Customer
{
private:
	std::string name;
	bool member = false;
	std::string memberType="None";
public:
	Customer(std::string name);
	Customer();
	std::string getName();
	bool isMember();
	void setMember(bool member);
	void setMemberType(std::string type);
	std::string getMemberType();
	std::string toString();
	~Customer();
};