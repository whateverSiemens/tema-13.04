#include "DiscountRate.h"


double DiscountRate::getServiceDiscountRate(std::string type)
{
	if (type == "Premium")
		return serviceDiscountPremium;
	else if (type == "Gold")
		return serviceDiscountGold;
	else if (type == "Silver")
		return serviceDiscountSilver;
	return 0;
}
double DiscountRate::getProductDiscountRate(std::string type)
{
	if (type == "Premium")
		return productDiscountPremium;
	else if (type == "Gold")
		return productDiscountGold;
	else if (type == "Silver")
		return productDiscountSilver;
	return 0;
}