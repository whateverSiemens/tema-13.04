#include "Visit.h"
#include "DiscountRate.h"

int main()
{
	Customer client1(std::string("Ionut"));

	Customer client2(std::string("Maria"));

	client2.setMember(true);
	client2.setMemberType(std::string("Gold"));
	Visit customer1visit(client1, boost::gregorian::date(2016,2,25));
	customer1visit.setProductExpense(23.25);
	customer1visit.setServiceExpense(25.12);
	Visit customer2visit(client2, boost::gregorian::date(2015, 3, 15));
	customer2visit.setProductExpense(100.25);
	customer2visit.setServiceExpense(85.12);

	std::cout << customer1visit.toString() << std::endl;
	std::cout << customer2visit.toString() << std::endl;

	return 0;
}