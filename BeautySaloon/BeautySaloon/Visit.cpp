#include "Visit.h"
#include "DiscountRate.h"
Visit::Visit(Customer& aName, boost::gregorian::date aDate)
{
	date = aDate;
	customer = aName;
}

Visit::~Visit()
{
	;
}

std::string Visit::getName()
{
	return customer.getName();
}

double Visit::getServiceExpense()
{
	return serviceExpense;
}

double Visit::getProductExpense()
{
	return productExpense;
}

void Visit::setServiceExpense(double ex)
{
	double discount = 0;
	if (customer.isMember())
	{
		DiscountRate dr;
		discount = ex*dr.getServiceDiscountRate(customer.getMemberType());
	}
	serviceExpense = ex - discount;
}
void Visit::setProductExpense(double ex)
{
	double discount = 0;
	if (customer.isMember())
	{
		DiscountRate dr;
		discount = ex*dr.getProductDiscountRate(customer.getMemberType());
	}
	productExpense = ex - discount;
}

double Visit::getTotalExpense()
{
	return getProductExpense()+getServiceExpense();
}
std::string Visit::toString()
{
	return customer.toString().append(" ").append(boost::gregorian::to_simple_string(date)).
		append(" ").append(std::to_string(getTotalExpense()));
}



