#pragma once
#include "Customer.h"
#include "boost\date_time\gregorian\gregorian.hpp"

class Visit
{

private:
	Customer customer;
	boost::gregorian::date date;
	double serviceExpense;
	double productExpense;
public:
	Visit(Customer& name, boost::gregorian::date date);
	std::string getName();
	double getServiceExpense();
	void setServiceExpense(double ex);
	double getProductExpense();
	void setProductExpense(double ex);
	double getTotalExpense();
	std::string toString();
	~Visit();
};