#include"CreditCard.h"
#include <string>

int main()
{
	Money m(1, 80);
	(m + m + m).print();

	std::map<std::string, double> map;
	CreditCard c(std::string("ionescu"), std::string("0012435"), map);

	c.charge(std::string("poseta"), m);
	c.charge(std::string("star wars movie"), 5, 10);

	c.print();
}