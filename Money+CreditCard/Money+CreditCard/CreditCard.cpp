#include"CreditCard.h"


CreditCard::CreditCard(std::string aOwnerName, std::string aCardNumber, std::map<std::string, double> aTransactions):
	mOwnerName(aOwnerName),
	mCardNumber(aCardNumber),
	mTransactions(aTransactions)
{
}

CreditCard::~CreditCard()
{
	;
}

void CreditCard::print()
{
	std::map<std::string, double>::iterator it;
	for (std::map<std::string, double>::iterator it = mTransactions.begin(); it != mTransactions.end(); ++it)
		std::cout << it->first<<"=>" << it->second << '\n';

}

void CreditCard::charge(std::string itemName, Money money)
{
	double price;
	price = (double)money.get_mEuros();
	price = price * 100 + (double)money.get_mCentimes();
	price /= 100;

	mTransactions[itemName] = price;

}


void CreditCard::charge(std::string itemName, int euros, int centimes)
{
	double price;
	price = (double)euros;
	price = price * 100 + (double)centimes;
	price /= 100;

	mTransactions[itemName] = price;
}