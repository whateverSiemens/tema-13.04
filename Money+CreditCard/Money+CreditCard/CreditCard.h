#pragma once
#include<iostream>
#include<map>
#include<string>
#include"Money.h"

class CreditCard
{
private:
	std::string mOwnerName;
	std::string mCardNumber;
	std::map<std::string, double> mTransactions;

public:
	CreditCard(std::string aOwnerName,
		std::string aCardNumber,
		std::map<std::string, double> aTransactions
	);

	~CreditCard();

	void print();

	void charge(std::string, Money);
	void charge(std::string, int, int);

};