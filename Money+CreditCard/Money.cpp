#include"Money.h"


Money::Money(int aEuros, int aCentimes):
mEuros(aEuros),
mCentimes(aCentimes)
{
	corectare();
}
Money::Money()
{
	
}

int Money::get_mEuros()
{
	return mEuros;
}
int Money::get_mCentimes()
{
	return mCentimes;
}

void Money::set_mEuros(int aEuros)
{
	mEuros = aEuros;
}
void Money::set_mCentimes(int aCentimes)
{
	mEuros = aCentimes;
	corectare();
}


Money Money::operator +(const Money& other)
{
	int total1 = mEuros * 100 + mCentimes;
	int total2 = other.mEuros * 100 + other.mCentimes;

	return Money((total1 + total2) / 100, (total1 + total2) % 100);
}
Money Money::operator -(const Money& other)
{
	int total1 = mEuros * 100 + mCentimes;
	int total2 = other.mEuros * 100 + other.mCentimes;

	if (total1 - total2 < 0)
	{
		std::cout << "Rezultatul nu poate fi negativ"<<std::endl;
		return Money(0,0);
	}
	return Money((total1 - total2) / 100, (total1 - total2) % 100);
}
Money Money::operator *(const int scalar)
{
	int total= mEuros * 100 + mCentimes;

	return Money((total*scalar)/100, (total*scalar) % 100);
}
Money Money::operator /(const int scalar)
{
	int total = mEuros * 100 + mCentimes;

	return Money((total/scalar) / 100, (total/scalar) % 100);
}
bool Money::operator ==(const Money& other)
{
	return mEuros == other.mEuros && mCentimes == other.mCentimes;
}
bool Money::operator !=(const Money& other)
{
	return !this->operator==(other);
}

void Money::corectare()
{
	if (mCentimes > 100)
	{
		mEuros += mCentimes / 100;
		mCentimes = mCentimes % 100;
	}
}

void Money::print()
{
	if(mCentimes<10)
		std::cout << mEuros << ",0" << mCentimes << " Euros" << std::endl;
	else
		std::cout << mEuros << "," << mCentimes<<" Euros"<<std::endl;
}
