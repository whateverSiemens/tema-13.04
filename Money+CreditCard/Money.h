#pragma once
#include<iostream>

class Money
{
private:
	int mEuros;
	int mCentimes;

public:
	Money(int aEuros, int aCentimes);
	Money();

	
	int get_mEuros();
	int get_mCentimes();

	void set_mEuros(int aEuros);
	void set_mCentimes(int aCentimes);


	Money operator +(const Money& other);
	Money operator -(const Money& other);
	Money operator *(const int);
	Money operator /(const int);

	bool operator ==(const Money& other);
	bool operator !=(const Money& other);

	void corectare();
	void print();
};